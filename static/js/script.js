function simpleTemplating(data) {
  var html = '';
  $.each(data, function(index, item){
    html += '<div class="w3-card-4 w3-margin w3-white"><img src="' + item.social_image + '" style="width:100%"><div class="w3-container w3-padding-8"><h3><b>' + item.title + '</b></h3><h5><span class="w3-opacity">' + item.readable_publish_date + '</span></h5></div>'
    html +='<div class="w3-container"><p>' + item.description + '</p><div class="w3-row"><div class="w3-col m8 s12"><p><a class="w3-btn w3-padding-large w3-white w3-border w3-hover-border-black" href="' + item.url + '" target="_blank" rel="noopener noreferrer"><b>READ MORE &raquo;</b></a></p></div></div></div></div>';
  });
  return html;
}

function articles(a) {
  $('#pagination-container').pagination({
    dataSource: a,
    pageSize: 5,
    callback: function(data, pagination) {
      var html = simpleTemplating(data);
      $('#data-container').html(html);
    }
  });
}
